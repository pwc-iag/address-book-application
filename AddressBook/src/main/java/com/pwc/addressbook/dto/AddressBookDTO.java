package com.pwc.addressbook.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddressBookDTO {

    private String name;
    private String phoneNumber;
    private String addressBookName;
}
