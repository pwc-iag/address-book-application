package com.pwc.addressbook.controller;

import com.pwc.addressbook.dto.AddressBookDTO;
import com.pwc.addressbook.entity.AddressBook;
import com.pwc.addressbook.service.AddressBookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/user/v1")
public class AddressBookController {
    private static final Logger log = LoggerFactory.getLogger(AddressBookController.class);
    @Autowired
    private AddressBookService addressBookService;

    @PostMapping("/add-new-contact")
    public String addNewContact(@ModelAttribute("addressBookDTO") AddressBookDTO addressBookDTO) {
        String response = addressBookService.addNewContact(addressBookDTO);

        return "redirect:/user/v1/get-all-contacts";
    }

    @GetMapping("/get-all-contacts-bookname")
    public String getAllContactsByAddressBookName(@ModelAttribute("addressBookName") String addressBookName, Model model) {
        List<AddressBookDTO> addressBookList = addressBookService.getAllContactsByAddressBookName(addressBookName);
        model.addAttribute("addressBookList", addressBookList);

        return "get-all-contacts-bookname";
    }

    @GetMapping("/get-all-contacts")
    public String getAllContacts(Model model) {
        model.addAttribute("addressBookList", addressBookService.getAllContacts());
        return "index";

    }

    @GetMapping("/show-add-contact-form")
    public String showAddContactForm(Model model) {
        AddressBookDTO addressBookDTO = new AddressBookDTO();
        model.addAttribute("addressBookDTO", addressBookDTO);
        return "add-contact-form";
    }

    @GetMapping("/show-get-contact-form-by-AddressBook")
    public String showGetContactFormByAddressBook(Model model) {
        // create model attribute to bind form data
        AddressBookDTO addressBookDTO = new AddressBookDTO();
        model.addAttribute("addressBookDTO", addressBookDTO);
        return "view-contact-form-by-address-book";
    }

    @GetMapping("/get-all-unique-relative-contacts")
    public String getAllUniqueContacts(Model model) {
        List<AddressBook> addressBookList = addressBookService.getAllUniqueContacts("AddressBookSamsung", "AddressBookGoogle");
        model.addAttribute("addressBookList", addressBookList);
        return "get-all-unique-relative-contacts";
    }
}
