package com.pwc.addressbook.service;

import com.pwc.addressbook.dto.AddressBookDTO;
import com.pwc.addressbook.entity.AddressBook;

import java.util.List;

public interface AddressBookService {
    public String addNewContact(AddressBookDTO addressBookDTO);

    public List<AddressBookDTO> getAllContactsByAddressBookName(String addressBookName);

    public List<AddressBookDTO> getAllContacts();

    public List<AddressBook> getAllUniqueContacts(String addBookName1, String addBookName2);
}
