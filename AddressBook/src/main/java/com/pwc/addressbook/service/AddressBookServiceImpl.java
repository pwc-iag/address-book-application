package com.pwc.addressbook.service;

import com.pwc.addressbook.dto.AddressBookDTO;
import com.pwc.addressbook.entity.AddressBook;
import com.pwc.addressbook.exception.ContactAlreadyExistsException;
import com.pwc.addressbook.exception.NoSuchAddressBookExistsException;
import com.pwc.addressbook.repository.AddressBookRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class AddressBookServiceImpl implements AddressBookService {

    private static final Logger log = LoggerFactory.getLogger(AddressBookServiceImpl.class);
    @Autowired
    private AddressBookRepository addressBookRepository;
    @Autowired
    private ModelMapper modelMapper;

    public int getMaxId() {
        return addressBookRepository.findAll().size() + 1;
    }

    public String addNewContact(AddressBookDTO addressBookDTO) {
        Optional<AddressBook> addressBookExisting = addressBookRepository.findByNameAndAddressBookName(addressBookDTO.getName(), addressBookDTO.getAddressBookName());
        if (Optional.empty().equals(addressBookExisting)) {
            AddressBook addressBook = new AddressBook();
            addressBook.setId(getMaxId());
            addressBook.setAddressBookName(addressBookDTO.getAddressBookName());
            addressBook.setName(addressBookDTO.getName());
            addressBook.setPhoneNumber(addressBookDTO.getPhoneNumber());
            addressBookRepository.save(addressBook);
            log.info("Contact Saved Successfully!");
            return "SUCCESS,Contact Saved";
        } else {
            throw new ContactAlreadyExistsException("Contact already present in AddressBook");
        }
    }

    public List<AddressBookDTO> getAllContactsByAddressBookName(String addressBookName) {
        if (addressBookName != null) {
            List<AddressBook> addressBookList = addressBookRepository.findAllByAddressBookName(addressBookName);

            if (!addressBookList.isEmpty()) {
                List<AddressBookDTO> addressBookDTOList = new ArrayList<>();

                for (AddressBook addressBook : addressBookList) {
                    addressBookDTOList.add(modelMapper.map(addressBook, AddressBookDTO.class));
                }
                log.info("Fetching contacts from DB by addressbook name!");
                return addressBookDTOList;
            } else {
                throw new NoSuchAddressBookExistsException("Not a valid Address Book Name:" + addressBookName);

            }
        } else {
            throw new NoSuchAddressBookExistsException("AddressBookName is null, please enter one");
        }
    }

    public List<AddressBookDTO> getAllContacts() {
        List<AddressBook> addressBookList = addressBookRepository.findAll();
        if (!addressBookList.isEmpty()) {
            List<AddressBookDTO> addressBookDTOList = new ArrayList<>();

            for (AddressBook addressBook : addressBookList) {
                addressBookDTOList.add(modelMapper.map(addressBook, AddressBookDTO.class));
            }
            log.info("fetching all contacts from DB!");
            return addressBookDTOList;
        } else {
            throw new NoSuchAddressBookExistsException("AddressBook is Empty");
        }
    }


    public List<AddressBook> getAllUniqueContacts(String addBookName1, String addBookName2) {

        List<AddressBook> addressBookList1 = addressBookRepository.findByAddressBookName(addBookName1);
        List<AddressBook> addressBookList2 = addressBookRepository.findByAddressBookName(addBookName2);
        if (!addressBookList2.isEmpty() && !addressBookList1.isEmpty()) {
            List<AddressBook> findIntersection = new ArrayList<>(addressBookList1);
            findIntersection.retainAll(addressBookList2);
            addressBookList1.removeAll(findIntersection);
            addressBookList2.removeAll(findIntersection);
            addressBookList1.addAll(addressBookList2);
            List<AddressBook> containsRelativeUnique = new ArrayList<>(addressBookList1);
            log.info("Fetching Unique Relative contacts!");
            return containsRelativeUnique;
        } else {
            throw new NoSuchAddressBookExistsException("UniqueContacts cannot be found as either or one or both address books are empty!");
        }
    }
}



