package com.pwc.addressbook.repository;

import com.pwc.addressbook.entity.AddressBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AddressBookRepository extends JpaRepository<AddressBook, Long> {
    @Query(value = "SELECT * FROM ADDRESS_BOOK ORDER BY name ASC", nativeQuery = true)
    List<AddressBook> findAll();

    @Query(value = "SELECT * FROM ADDRESS_BOOK WHERE ADDRESS_BOOK_NAME=?1 ORDER BY name ASC", nativeQuery = true)
    List<AddressBook> findAllByAddressBookName(String addressBookName);

    @Query(value = "SELECT * FROM ADDRESS_BOOK  WHERE ADDRESS_BOOK_NAME = ?2 AND NAME = ?1", nativeQuery = true)
    Optional<AddressBook> findByNameAndAddressBookName(String name, String addressBookName);

    List<AddressBook> findByAddressBookName(String addressBookName);
}
