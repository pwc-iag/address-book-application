package com.pwc.addressbook.service;

import com.pwc.addressbook.dto.AddressBookDTO;
import com.pwc.addressbook.entity.AddressBook;
import com.pwc.addressbook.exception.ContactAlreadyExistsException;
import com.pwc.addressbook.exception.NoSuchAddressBookExistsException;
import com.pwc.addressbook.repository.AddressBookRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AddressBookServiceImplTest {
    @Mock
    private AddressBookRepository addressBookRepository;
    @Spy
    private ModelMapper modelMapper;
    @InjectMocks
    private AddressBookServiceImpl addressBookServiceImpl;

    @Test
    void addNewContactTest() {
        String response = "SUCCESS,Contact Saved";

        AddressBookDTO addressBookDTO = new AddressBookDTO();
        addressBookDTO.setName("test");
        addressBookDTO.setAddressBookName("testBook");
        Optional<AddressBook> optionalAddressBook = Optional.empty();
        when(addressBookRepository.findByNameAndAddressBookName(Mockito.anyString(), Mockito.anyString())).thenReturn(optionalAddressBook);
        Assertions.assertEquals(response, addressBookServiceImpl.addNewContact(addressBookDTO));
    }

    @Test
    void addNewContactTest_throwsException() {
        AddressBookDTO addressBookDTO = new AddressBookDTO();
        addressBookDTO.setName("test");
        addressBookDTO.setAddressBookName("testBook");
        AddressBook addressBook = new AddressBook();
        addressBook.setName("test234");
        addressBook.setAddressBookName("test546");
        Optional<AddressBook> optionalAddressBook = Optional.ofNullable(addressBook);
        when(addressBookRepository.findByNameAndAddressBookName(Mockito.anyString(), Mockito.anyString())).thenReturn(optionalAddressBook);
        Assertions.assertThrows(ContactAlreadyExistsException.class, () -> {
            addressBookServiceImpl.addNewContact(addressBookDTO);
        });

    }

    @Test
    void getAllContactsTest() {

        AddressBook addressBook = new AddressBook(1, "Shankha", "AddressBookGoogle", "7980725872");
        List<AddressBook> testList = new ArrayList<>();
        testList.add(addressBook);
        when(addressBookRepository.findAll()).thenReturn(testList);
        List<AddressBookDTO> li = addressBookServiceImpl.getAllContacts();
        Assertions.assertNotNull(addressBookServiceImpl.getAllContacts());

        Assertions.assertEquals("Shankha", li.get(0).getName());
    }

    @Test
    void getAllUniqueContactsTest() {
        String addBookName1 = "google";
        String addBookName2 = "ios";
        AddressBook addressBook = new AddressBook();
        List<AddressBook> addressBookList = new ArrayList<>();
        addressBookList.add(addressBook);
        when(addressBookRepository.findByAddressBookName(Mockito.anyString())).thenReturn(addressBookList);
        Assertions.assertNotNull(addressBookServiceImpl.getAllUniqueContacts(addBookName1, addBookName2));
    }

    @Test
    void getAllContactsByAddressBookNameTest() {
        String addressBookName = "test";
        AddressBook addressBook = new AddressBook();
        List<AddressBook> addressBookList = new ArrayList<>();
        addressBookList.add(addressBook);
        when(addressBookRepository.findAllByAddressBookName(Mockito.anyString())).thenReturn(addressBookList);
        Assertions.assertNotNull(addressBookServiceImpl.getAllContactsByAddressBookName(addressBookName));
    }

    @Test
    void getAllContactsByAddressBookNameTest_throwsException() {
        String addressBookName = "test";
        List<AddressBook> addressBookList = new ArrayList<>();
        when(addressBookRepository.findAllByAddressBookName(Mockito.anyString())).thenReturn(addressBookList);
        Assertions.assertThrows(NoSuchAddressBookExistsException.class, () -> {
            addressBookServiceImpl.getAllContactsByAddressBookName(addressBookName);
        });
    }
}
